# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [libdap])
m4_define([AT_PACKAGE_TARNAME],   [libdap])
m4_define([AT_PACKAGE_VERSION],   [3.19.1])
m4_define([AT_PACKAGE_STRING],    [libdap 3.19.1])
m4_define([AT_PACKAGE_BUGREPORT], [opendap-tech@opendap.org])
